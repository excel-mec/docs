# Kryptos Backend

[Kryptos Backend](https://github.com/Excel-MEC/excelplay-backend-kryptos) is the backend for Kryptos, the online treasure hunt. It's written in [Golang](https://golang.org/).

Kryptos Backend broadly follows the MVC pattern and provides a REST API for the client.

## Directory Structure

- `cmd` - Setup and initializaton of the server
- `pkg` -
  - `database` - Database schema and wrappers around queries
  - `env` - Constants used for configuration
  - `handlers` - Handlers/Controllers for each API endpoint.
  - `httperrors` - Wrapper around common http errors
  - `middlewares` - Middlewares for auth, logging etc
  - `routes` - The API endpoints

### Middlewares

#### Auth Middleware

Kryptos uses JWT for authentication. Handlers of endpoints using Auth middleware will be executed only if the request contains an `Authorization` header with a valid JWT in the format `Bearer <jwt>`.

#### Error Handling Middleware

`Logrus` is used to log requests with errors, and send the error back in the response. Makes use of `httperrors`.

#### Logger Middleware

`Logrus` is used to log all incoming requests. This middleware is applied to every API endpoint.
