# API

The project follows the [repository pattern](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/infrastructure-persistence-layer-design). The different project layers are 

- Controller layer
- Business Logic
- Repository Classes
- Data Access Layer built using `Entity Framework CORE`
- Database server

The  different layers are shown the diagram below:

![Dotnet layer](../../../assets/DotnetLayers.png)

## Packages

Given is a list of packages/dependencies of the API Project.

- Microsoft.EntityFrameworkCore
- Microsoft.EntityFrameworkCore.Design 
- AutoMapper.Extensions.Microsoft.DependencyInjection
- Npgsql.EntityFrameworkCore.PostgreSQL.Design
- System.IdentityModel.Tokens.Jwt
- Microsoft.IdentityModel.Tokens
- Microsoft.AspNetCore.Authentication.JwtBearer
- Npgsql.EntityFrameworkCore.PostgreSQL 
- Google.Cloud.Storage.V1
- Swashbuckle.AspNetCore
- QRCoder
- Swashbuckle.AspNetCore.Annotations
- Microsoft.Extensions.Http

 Look [here](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/API.csproj) to know more about the packages used.



## Database

Database used in this project is `PostgreSQL`. Data models can be viewed [here](/Accounts/Backend/API/Models/Models).

## Program Class

The program execution start from the program class.

### Initial seeding of data

At the program startup, the empty database is populated with a list of colleges and schools from the `SeedCollege` and `SeedSchool` files in the Helpers directory.



## Startup Class

Program Class transfers the control of execution to the Startup Class. This contain methods to add services to the Ioc container and to configure the middleware pipeline.

### ConfigureServices Method

The ConfigureServices method is a place where you can register your dependent classes with the built-in `IoC container`. Given below are the registered dependant classes / services.

#### AddControllers 

Adds the [controllers](/Accounts/Backend/API/Controllers/Controllers.md) which contain the API endpoints.

#### AddDbContext

Adds the [database](/Accounts/Backend/API/API?id=database) as service.

#### AddCustomServices

Adds the [custom services](/Accounts/Backend/API/Services/Services?id=custom-services) which contain the business logic.

#### AddRepositoryServices

Adds the [repository classes](/Accounts/Backend/API/Data/Data?id=repository-services) which comes under the repository layer.

#### AddAutoMapper

Adds the [Automapper services](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Helpers/AutoMapperProfiles.cs) which is to map objects of different types.

#### AddAuthentication

Adds the `jwt` Authentication service

#### AddSwaggerGen

Adds the SwaggerGen service which is a [Swagger](https://swagger.io/) generator, defining 1 or more Swagger documents.



## Middlewares

[Middleware](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-3.1) is software that's assembled into an app pipeline to handle requests and responses. Given below are the components registered in the middleware pipeline.

### ConfigureExceptionHandlerMiddleware

Custom Exception handler. Learn more about it [here](/Accounts/Backend/API/Extensions/Extensions?id=configureexceptionhandlermiddleware).

### UseSwagger 

Enable middleware to serve generated [Swagger](https://swagger.io/) as a JSON endpoint.

### UseSwaggerUI

Enable middleware to serve Swagger-UI (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.

### Database.Migrate

Automate the [database](/Accounts/Backend/API/API?id=database) updation.

### UseCors

Allows cross origin requests abide by the [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing) policy defined in the project.

### UseRouting

`UseRouting` adds the route matching to the middleware pipeline from the set of endpoints defined in the app.

### UseEndpoints

`UseEndpoints` adds endpoint execution to the middleware pipeline. 

### UseAuthentication and UseAuthorization

API uses JWT for authentication. Handlers of endpoints using this middleware will be executed only if the request contains an `Authorization` header with a valid JWT in the format `Bearer<jwt>.`



