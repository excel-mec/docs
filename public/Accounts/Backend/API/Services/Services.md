## Custom Services

### Auth Service

Custom Auth Service and HttpClient Service.

#### FetchUserFromAuth0

Method to fetch user from [`Auth0`](https://auth0.com/).

```c#
Task<string> FetchUserFromAuth0(string access_token);
```

#### CreateJwtForClient

Method to create a `jwt` token for the client.

```c#
Task<string> CreateJwtForClient(string responseString);
```



### Profile Service

Custom Profile Service.

#### UploadProfileImage

Method to upload profile image of the user to `Google Cloud Storage`.

```c#
Task<string> UploadProfileImage(DataForProfilePicUpdateDto data);
```



### GoogleCloudStorage Service

Google Cloud Storage as a service.

#### UploadFileAsync

Method to upload the given imageFile to the `Google Cloud Storage` with the given fileNameForStorage as the object name.

```c#
Task<string> UploadFileAsync(IFormFile imageFile, string fileNameForStorage);
```

#### DeleteFileAsync

 Method to delete the file with the given fileNameForStorage as the object name from the `Google Cloud Storage bucket`.

```c#
Task DeleteFileAsync(string fileNameForStorage);
```



### QRCode Generation service

QRCode generation as a service.

#### CreateQrCode

Method to create a QRCode for the given Excel ID.

```c#
Task<string> CreateQrCode(string id);
```



### Cipher Service

Service for encryption and decryption.

#### Encryption

Method to encrypt the given plain text with an encryption key.

```c#
string Encryption(string key, string textToBeEncrypted);
```

#### Decryption

Method to decrypt the given cipher text with an encryption key.

```c#
string Decryption(string key, string cipherText);
```



Check out more about custom services from [here](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/tree/master/API/Services).