# Controllers

The Controllers contain the API endpoints. Controllers handles incoming HTTP requests, retrieves necessary model data and returns appropriate responses.

### AuthController

[AuthController](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Controllers/AuthController.cs) contains the endpoints related to the repository class [AuthRepository](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Data/AuthRepository.cs).

#### Login

Method which handles user login.



#### ProfileController

[ProfileController](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Controllers/InstitutionController.cs) contains the endpoints related to the repository class [ProfileRepository](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Data/InstitutionRepository.cs).

#### UpdateProfile

Updates the profile of a user( *except Profile image)*.

#### UpdateProfileImage

Updates the profile image of a user.

#### View

Returns the Profile details of a user.



### InstitutionController

[InstitutionController](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Controllers/InstitutionController.cs) contains the endpoints related to the repository class [InstitutionRepository](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Data/InstitutionRepository.cs).

#### College List

Returns a list of colleges.

#### AddCollege

Adds a new college.

#### SchoolList

Returns a list of schools.

#### AddSchool

Adds a new school.



### TestController

[TestController](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/blob/master/API/Controllers/TestController.cs) contains the endpoints for the purpose of testing.



Check out more about controllers from [here](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend/-/tree/master/API/Controllers).